import { dirname } from "path"
import path = require("path")

// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */

interface KnexConfig {
  [key: string]: object
}

// let dataBasePath: string
// let pathSplit:string[] = __dirname.split(`/`)
// if (pathSplit.length === 1) {
//   pathSplit = __dirname.split(`\\`)
// }
// if (pathSplit.includes('dist')) {
//   dataBasePath = path.join(__dirname, '..', '..', 'db', 'DataBase.sqlite3')
// } else {
//   dataBasePath = './DataBase.sqlite3'
// }

export default <KnexConfig> {
  development: {
    client: 'pg',
    connection: {
      host: "89.23.100.86",
      user: 'postgres',
      password: '3251',
      port: 5432,
      database: 'caseguru_dev',
      
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: "./migrations"
    }
  },
};
