import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('hiring', function(table: Knex.CreateTableBuilder) {
        table.increments('id').primary()
        table.string('lastName').notNullable()
        table.string('firstName').notNullable()
        table.string('secondName').notNullable()
        table.float('pay').notNullable()
        table.integer('role').notNullable()
        table.timestamp('created_at').notNullable()
        table.timestamp('updatedAt').notNullable()
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('hiring')
}

