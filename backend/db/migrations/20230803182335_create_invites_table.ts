import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('invites', function(table: Knex.CreateTableBuilder) {
        table.increments('id').primary()
        table.integer('idUser').unsigned()
        table.foreign('idUser').references('users.id').onDelete('cascade');
        table.string('token').notNullable()
        table.dateTime('created_at').notNullable()
        table.dateTime('updatedAt').notNullable()
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('invites')
}

