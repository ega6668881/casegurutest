import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { ValidatorOptions } from "class-validator";


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.setGlobalPrefix('api')
  app.enableVersioning()
  app.enableCors()
  
  await app.listen(4000);
}
bootstrap();
