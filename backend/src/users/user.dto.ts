import { IsNotEmpty, IsOptional, IsString, IsNumber, IsDateString, MinLength, Matches } from "class-validator"

export class GetUserByIdDto {
    @IsNumber()
    @IsNotEmpty()
    readonly id: number
}

export class LoginDto {
    @IsString()
    @IsNotEmpty()
    readonly fullName:string
    @IsString()
    @IsNotEmpty()
    readonly password:string
}

export class DeleteUserDto {
    @IsNotEmpty()
    @IsNumber()
    readonly id:number
}

export class UpdateUserDto {
    @IsNotEmpty()
    @IsNumber()
    readonly id:number
    @IsNotEmpty()
    @IsString()
    readonly lastName:string
    @IsNotEmpty()
    @IsString()
    readonly firstName:string
    @IsNotEmpty()
    @IsString()
    readonly secondName:string
    @IsNotEmpty()
    @IsNumber()
    readonly pay:number
}

export class AddUserDto {
    @IsString()
    @IsNotEmpty()
    readonly lastName:string
    @IsString()
    @IsNotEmpty()
    readonly firstName:string
    @IsString()
    @IsNotEmpty()
    readonly secondName:string
    @IsNumber()
    @IsNotEmpty()
    readonly pay:number
    @IsNumber()
    @IsNotEmpty()
    readonly role:number
    @IsDateString()
    @IsNotEmpty()
    readonly birthday:Date
    @IsDateString()
    @IsNotEmpty()
    readonly created_at:Date
}

export class AddPasswordDto {
    @IsString()
    @IsNotEmpty()
    @MinLength(8)
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {message: 'Пароль слишком простой'})
    readonly password:string
    @IsString()
    @IsNotEmpty()
    readonly confirmPassword:string
    @IsNumber()
    @IsNotEmpty()
    readonly idUser:number
}