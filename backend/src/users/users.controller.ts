import { Controller, Get, Post, Body, ValidationPipe, Headers, Param, Put, Delete } from '@nestjs/common';
import { GetUserByIdDto, AddUserDto, AddPasswordDto, UpdateUserDto, LoginDto, DeleteUserDto } from './user.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private readonly userService: UsersService) {}


    @Get('get-user/:id')
    async getUserById(@Param('id') id:number) {
        return this.userService.GetUserById(id)
    }

    @Delete('delete-user/:id')
    async deleteUser(@Param('id') id: string) {
        return this.userService.DeleteUser(id)
    }

    @Put('update-user')
    async updateUser(@Body() dto: UpdateUserDto) {
        return this.userService.UpdateUser(dto)
    }

    @Get('get-users')
    async getUsers() {
        return this.userService.getUsers()
    }

    @Get('check-auth')
    async checkAuth(@Headers('jwt') jwtToken: string) {
        return this.userService.verifyToken(jwtToken)
    }
    
    @Get('get-users-birthday-last-month')
    async getMembersLastMonth() {
        return this.userService.getUsersBirthDayLastMonth()
    }

    @Get('invites/:token')
    async getInviteUser(@Param('token') token:string) {
        return this.userService.getUserByToken(token)
    }

    @Get('get-emp-year')
    async getEmpLastYear() {
        return this.userService.GetEmpLastYear()
    }

    @Post('add-password')
    async addPassword(@Body() dto: AddPasswordDto) {
        return this.userService.addPassword(dto.idUser, dto.password, dto.confirmPassword)
    }

    @Post('login')
    async login(@Body() dto: LoginDto) {
        return this.userService.login(dto.fullName, dto.password)
    }

    @Post()
    async create(@Body() dto:AddUserDto) {
        return this.userService.AddUser(dto)
    }
}
