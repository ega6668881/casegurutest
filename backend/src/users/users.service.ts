import { Users } from './users';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import knex, { Knex } from 'knex';
import knexfile from '../../db/knexfile'
import { AddUserDto, DeleteUserDto, UpdateUserDto } from './user.dto';
import { HttpExceptionBody } from '@nestjs/common/interfaces';
import { generateRandomString } from "ts-randomstring/lib"
import { genSaltSync, hashSync, compareSync } from "bcrypt";
import jwt from 'jsonwebtoken'
import * as dotenv from 'dotenv-ts'

dotenv.config()
export interface IUser {
    id?:number
    lastName:string
    firstName:string
    secondName:string
    pay:number
    role:number
    password?:string
    birthday:Date
    created_at:Date
    updatedAt:Date
}

interface DismissalUser extends Omit<IUser, 'password' | 'birthday'> {id?:number}

interface IHiring {
    id?:number
    lastName:string
    firstName:string
    secondName:string
    pay:number
    role:number
    created_at:Date
    updatedAt:Date
}

interface IInvites {
    id?:number
    idUser:number
    token:string
    created_at:Date
    updatedAt:Date
}

@Injectable()
export class UsersService {
    salt: string
    knex: Knex
    constructor() {
        this.knex = knex(knexfile.development)
        this.salt = genSaltSync(10)
        
    }

    private async generateJwtToken(idUser:number): Promise<string> {
        return jwt.sign({idUser: idUser}, process.env.TOKEN_SECRET, { expiresIn: '1h' })
    }


    private async comparePassword(password: string, hashPassword: string): Promise<boolean> {
        return compareSync(password, hashPassword)
    }

    private async hashPassword(password: string): Promise<string> {
        return hashSync(password, this.salt)
    }

    private async getInviteByUserId(idUser: number): Promise<IInvites[]> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            const invites = await this.knex('invites').select('*').where({idUser: idUser})
            return invites
        })
        return transaction
    }
    
    private async GetUserByFullName(lastName: string, firstName: string, secondName: string): Promise<object[]> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            return await this.knex<IUser>('users').select("*").where({firstName: firstName, lastName: lastName, secondName: secondName})
        })
        return transaction
    }

    async DeleteUser(id: string): Promise<object> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            const [firstUser] = await this.GetUserById(Number(id))
            if (!firstUser) {
                throw new HttpException("Пользователь не найден", HttpStatus.NOT_FOUND)
                
            } else {                 
                delete firstUser.birthday
                delete firstUser.password
                delete firstUser.id
                firstUser.created_at = new Date()
                const dismissal: DismissalUser = firstUser
                await this.knex('users').delete().where({id: Number(id)})
                await this.knex('dismissal').insert(dismissal).returning('*')
                return {success: true}
            }
            
        })

        return transaction
    }

    async getUsers(): Promise<object[]> {
        return await this.knex('users').select('*')
    }

    async GetEmpLastYear(): Promise<object> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction): Promise<object> => {
            
            const hiring = await this.knex('hiring').select('*', this.knex.raw(`EXTRACT(MONTH FROM created_at::date) as month`))
            const dismiss = await this.knex('dismissal').select('*', this.knex.raw(`EXTRACT(MONTH FROM created_at::date) as month`))
            return {hiring: hiring, dismiss: dismiss}
        })
        return transaction
    }

    async GetUserById(id: number): Promise<IUser[]> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            const users = await this.knex<IUser>('users').select('*').where({id: id})
            return users

        })
        return transaction
        
    }

    async getUsersBirthDayLastMonth(): Promise<object[]> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            const users = this.knex<IUser[]>('users').where('birthday', '>=', this.knex.raw(`CURRENT_DATE - '1 MONTH'::INTERVAL`))
            return users
        })
        return transaction
    }

    async verifyToken(token: string): Promise<object> {
        let checkToken: boolean
        let decoded: any = null
        jwt.verify(token, process.env.TOKEN_SECRET, function(err:string, info:any): boolean {
            if (err) {
                return checkToken = false
            } else {
                decoded = info
                return checkToken = true
            }
            
        })
        if (decoded) {
            const [firstUser] = await this.GetUserById(decoded.idUser)
            return {user: firstUser, check: true}
        }
        return {check: false}
    }

    async login(fullName: string, password: string): Promise<object> {
        const splitFullName: string[] = fullName.split(" ")
        if (splitFullName.length < 3) {
            throw new HttpException("Некоректное ФИО", HttpStatus.CONFLICT)
        }
        const users:any = await this.GetUserByFullName(splitFullName[0], splitFullName[1], splitFullName[2])
        if (users.length === 0 || users[0].password === null) {
            throw new HttpException("Пользователь не найден", HttpStatus.NOT_FOUND)

        } else {
            if (await this.comparePassword(password, users[0].password)) {
                const token = await this.generateJwtToken(users[0].id)
                return {user: users[0], jwtToken: token}

            } else {
                throw new HttpException("Неверный пароль", HttpStatus.CONFLICT)
            }
        }
    }

    async UpdateUser(dto: UpdateUserDto): Promise<object[]> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            const users = await this.GetUserById(dto.id)
            if (users.length === 0) {
                throw new HttpException("Пользователь не найден", HttpStatus.NOT_FOUND)
            } else {
                const updatedUser = await this.knex<IUser>('users').update({lastName: dto.lastName,
                    firstName: dto.firstName, secondName: dto.secondName, pay: dto.pay}).where({id: dto.id}).returning('*')

                return updatedUser
            }
        })

        return transaction
    }

    async addPassword(idUser:number, password:string, confirmPassword:string): Promise<object> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            const users:any = await this.GetUserById(idUser)
            if (users.length > 0) {
                const invites = await this.getInviteByUserId(idUser)
                if (invites.length === 0) {
                    throw new HttpException("Приглашение не найдено", HttpStatus.NOT_FOUND)
                }
                if (password === confirmPassword) {
                    const hashPassword: string = await this.hashPassword(password)
                    const updatedUser = await this.knex('users').update({password: hashPassword}).where({id: users[0].id}).returning('*')
                    await this.knex('invites').delete().where({id: invites[0].id})
                    const token: string = await this.generateJwtToken(users[0].id)
                    return {user: updatedUser[0], jwtToken: token}

                } else {
                    throw new HttpException("Пароли не совпадают", HttpStatus.CONFLICT)
                }

            } else {
                throw new HttpException("Пользователь не найден", HttpStatus.NOT_FOUND)
            }
        })
        return transaction
    }

    async getUserByToken(token: string): Promise<object[]> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            const invite: IInvites = await this.knex<IInvites>('invites').where({token: token}).first()
            if (invite) {
                const user = await this.knex<IUser>('users').select('*').where({id: invite.idUser})
                return user
            } else {
                throw new HttpException("Приглашение не найдено", HttpStatus.NOT_FOUND)
            }
            
        })
        return transaction
    }

    async GetUsersByRole(role: number): Promise<object[]> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            const users = await this.knex<IUser>('users').select('*').where({role: role})
            return users
        })
        return transaction
    }

    async GenerateInviteToken(): Promise<string> {
        return generateRandomString()
    }

    async AddUser(insertData: AddUserDto): Promise<HttpException | object | string> {
        const transaction = this.knex.transaction(async (trx: Knex.Transaction) => {
            const user: object[] = await this.GetUserByFullName(insertData.lastName, insertData.firstName, insertData.secondName)
            if (user.length > 0) {
                throw new HttpException(['Пользователь уже существует'], HttpStatus.FOUND)
            } else {
                const insertedUserData: IUser = {
                    lastName: insertData.lastName,
                    firstName: insertData.firstName,
                    secondName: insertData.secondName,
                    pay: insertData.pay,
                    role: insertData.role,
                    birthday: insertData.birthday,
                    created_at: insertData.created_at,
                    updatedAt: new Date()
                }
                const insertHiringData: IHiring = {
                    lastName: insertData.lastName,
                    firstName: insertData.firstName,
                    secondName: insertData.secondName,
                    pay: insertData.pay,
                    role: insertData.role,
                    created_at: insertData.created_at,
                    updatedAt: new Date()
                }
               
                await this.knex('hiring').insert(insertHiringData)
                const insertedUser: IUser[] = await this.knex('users').insert(insertedUserData).returning('*')
                const insertInviteData: IInvites = {
                    idUser: insertedUser[0].id,
                    token: await this.GenerateInviteToken(),
                    created_at: new Date(),
                    updatedAt: new Date()
                }
                await this.knex('invites').insert(insertInviteData)
                const inviteUrl:string = `http://127.0.0.1:3000/invite/${insertInviteData.token}`
                return {insertedUser: insertedUser[0], inviteUrl: inviteUrl}
            }
        })
        return transaction
    }
}
