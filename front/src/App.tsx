import './App.css';
import RoutesComponent from './components/routes/routes';
import NoficationComponent from './components/nofication/nofication';

function App() {
  return (
    <>
      <NoficationComponent />
      <RoutesComponent />
    </>
  )
}

export default App;
