import React, { FC, useState } from 'react'
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { observer } from 'mobx-react-lite'
import dayjs, { Dayjs } from 'dayjs';
import { Box, Button, Checkbox, Container, FormControlLabel, Grid, Link, TextField, Typography } from '@mui/material';
import { DateField } from '@mui/x-date-pickers/DateField';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import clientStore from '../../stores/clientStore';

const AddUserForm: FC<{}> = ({}) => {
    const [created_at, setcreated_at] = useState<Dayjs | null>(dayjs(new Date().toISOString()));
    const [birthday, setBirthday] = useState<Dayjs | null>(dayjs(new Date().toISOString()));
    const [fullName, setFullName] = useState<string>("")
    const [errorMessage, setErrorMessage] = useState<string[]>()
    const [userAdded, setUserAdded] = useState<string | null>(null)
    const [pay, setPay] = useState<number>(0)
    if (userAdded) {
        return (
        <Container component="main" sx={{maxWidth: "70%"}}>
        <Box
          sx={{
            boxShadow: 3,
            borderRadius: 2,
            px: 4,
            py: 6,
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
            <Typography variant="h5">
              Отправьте инвайт ссылку сотруднику
            </Typography>
            <Typography variant="h6">
              {userAdded}
            </Typography>
        </Box>
        </Container>
        )
    }
    return (
        <Container component="main" maxWidth="sm">
        <Box
          sx={{
            boxShadow: 3,
            borderRadius: 2,
            px: 4,
            py: 6,
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="h5">
            Добавление пользователя
          </Typography>
          <Box component="form" noValidate sx={{ mt: 1 }} onSubmit={(e) => clientStore.addUser(e, fullName, pay, created_at, birthday, setErrorMessage, setUserAdded)}>
            <TextField
                margin="normal"
                required
                fullWidth
                id="fullName"
                label="ФИО"
                name="fullName"
                autoComplete="text"
                onChange={(e) => setFullName(e.target.value)}
                autoFocus />
            <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="Заработная плата"
                  label="ЗП"
                  type="number"
                  onChange={(e) => setPay(Number(e.target.value))}
                  id="password"
                  autoComplete="current-password"/>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DemoContainer components={['DateField']} sx={{display: "flex"}}>
                <DateField sx={{display: "flex", width: "100%"}}
                label="Дата найма"
                value={created_at}
                onChange={(newValue) => setcreated_at(newValue)}
                />
            </DemoContainer>
            <DemoContainer components={['DateField']} sx={{display: "flex"}}>
                <DateField sx={{display: "flex", width: "100%"}}
                label="Дата рождения"
                value={birthday}
                onChange={(newValue) => setBirthday(newValue)}
                />
            </DemoContainer>
            </LocalizationProvider>
              <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}>
                  Добавить пользователя
              </Button>
              <Typography component="h1" variant="h6" sx={{fontSize: "13px"}}>
                {errorMessage && (errorMessage.map(error => <p>{error}</p>))}
              </Typography>
            
          </Box>
        </Box>
      </Container>
    )
}

export default observer(AddUserForm)