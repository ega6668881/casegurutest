import React, { FC, PropsWithChildren, useEffect, useState } from 'react'
import { Box, Button, Checkbox, Container, FormControlLabel, Grid, Link, TextField, Typography, CircularProgress} from '@mui/material';
import clientStore from '../../stores/clientStore';
import Loading from '../loading/loading';
import LoginPage from '../../screens/Login';
import { observer } from 'mobx-react-lite';
import NotFound from '../notFound/notFound';

type PropsTypes = {
  Component: React.JSX.Element
  roles?: number[] | undefined
}


const CheckAuth: FC<PropsTypes> = ({Component, roles}) => {
  const [isLoading, setLoading] = useState(true)
  
  useEffect(() => {
    clientStore.checkAuth(setLoading)
  }, [])
  if (isLoading) {
    return <Loading />

  } else if (clientStore.user && clientStore.isLogin) {
    if (roles && !roles.includes(clientStore.user.role)) {
      return <NotFound />
    }

    return Component
  } else {
    return <LoginPage ReturnComponent={Component} />
  }

}

export default observer(CheckAuth)