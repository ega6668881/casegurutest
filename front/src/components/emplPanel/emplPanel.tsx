import { FC} from 'react'
import { observer } from 'mobx-react-lite';
import { Typography, Box, Container } from '@mui/material';
import EmpList from '../hrPanel/empList/empList';
import clientStore from '../../stores/clientStore';
import './style.css'

interface UserFullName {
    lastName: string | undefined
    firstName: string | undefined
    secondName: string | undefined
}

const EmpPanel: FC<{}> = ({}) => {
    const {user} = clientStore
    const fullName: UserFullName = {
        lastName: user?.lastName,
        firstName: user?.firstName,
        secondName: user?.secondName
    }
    return (
        <div className='emp-panel-wrapper'>
            <EmpList edit={false}/>
            <Container component="main" maxWidth="sm">
            <Box
                sx={{
                boxShadow: 3,
                borderRadius: 2,
                px: 4,
                py: 6,
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                gap: 3,
                }}
            >
            <Typography>{`${fullName.lastName} ${fullName.firstName} ${fullName.secondName}`}</Typography>
            <Typography>Зароботная плата {`${user?.pay}р`}</Typography>
            <Typography>Дата найма {new Date(user?.created_at || '').toISOString().split('T')[0]}</Typography>
            <Typography>Дата рождения {new Date(user?.birthday || '').toISOString().split('T')[0]}</Typography>
            </Box>
            </Container>
        </div>
    )
}   

export default observer(EmpPanel)