import React, { FC } from 'react'
import { AppBar, Container, Toolbar, IconButton, Typography, Box, Button } from '@mui/material'; 
import { Link } from 'react-router-dom';
import MenuIcon from '@mui/icons-material/Menu'
import './style.css'

const Header: FC<{}> = ({}) => {
    return (
        <AppBar>
        <div className='header-wrapper'>
            <Toolbar className='toolbar'>
                <Typography variant="h6">
                    X-company
                </Typography>
                <div className='auth-buttons-wrapper'>                    
                    <Link to={"/login"}><Button variant="contained" color='success'>Вход</Button></Link>
                </div>
            </Toolbar>
        </div>
        </AppBar>
    )
}

export default Header