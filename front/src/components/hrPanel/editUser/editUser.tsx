import { FC, useState, useEffect } from 'react'
import { observer } from 'mobx-react-lite';
import { Typography, Box, Container, TextField, Button } from '@mui/material';
import clientStore, { IUser } from '../../../stores/clientStore';
import './style.css'
import { Navigate } from 'react-router-dom';

type PropsType = {
    user: IUser
}

const EditUser: FC<PropsType> = ({user}) => {
    const [fullName, setFullname] = useState<string>(`${user?.lastName} ${user?.firstName} ${user?.secondName}`)
    const [pay, setPay] = useState(user?.pay)
    const [editedUser, setEditedUser] = useState<boolean>(false)

    if (editedUser) {
        return <Navigate to={`/`}/>
    }

    return (
        <div className='emp-panel-wrapper'>
            <Container component="main" maxWidth="sm">
            <Box component="form"
                sx={{
                boxShadow: 3,
                borderRadius: 2,
                px: 4,
                py: 6,
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                gap: 1,
                }} onSubmit={(e) => clientStore.updateUser(e, user?.id, fullName, pay, setEditedUser)}>
            <Typography>Редактирование пользователя</Typography>
            <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="fullName"
                    label="ФИО"
                    name="fullName"
                    defaultValue={`${user?.lastName} ${user?.firstName} ${user?.secondName}`}
                    autoComplete="text"
                    autoFocus
                    onChange={(e) => setFullname(e.target.value)}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="pay"
                    label="Зароботная плата"
                    defaultValue={user?.pay}
                    type="number"
                    id="pay"
                    onChange={(e) => setPay(Number(e.target.value))}
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}>
                    Применить изменения
                </Button>
            </Box>
            </Container>

        </div>
    )
}   

export default observer(EditUser)