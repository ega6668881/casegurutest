import React, { FC, useEffect, useState } from 'react'
import { DataGrid, GridCellParams, GridColDef, GridRenderCellParams, GridValueGetterParams } from '@mui/x-data-grid';
import Button from '@mui/material/Button/Button';
import clientStore from '../../../stores/clientStore';
import Loading from '../../loading/loading';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';

type PropsType = {
    edit: boolean
}



const EmpList: FC<PropsType> = ({edit}) => {
    const [isLoading, setLoading] = useState(true)
    useEffect(() => {
        clientStore.getUsers(setLoading)
    }, [isLoading])

    if (isLoading) {
        return <Loading />
    }

    const columns: GridColDef[] = [
        { field: 'lastName', headerName: 'Фамилия', width: 200 },
        { field: 'firstName', headerName: 'Имя', width: 200 },
        { field: 'secondName', headerName: 'Отчество', width: 200 },
        {
          field: 'pay',
          headerName: 'Заработная плата',
          type: 'number',
          width: 180,
        },
        {
          field: 'created_at',
          headerName: 'Дата найма',
          width: 160,
          valueGetter: (params: GridValueGetterParams) =>
            `${params.row.created_ad || ''} ${new Date(params.row.created_at).toISOString().split('T')[0] || ''}`,
        }];
    
    if (edit) {
        columns.push({ field: 'id', headerName: '', width: 220, renderCell: (params: GridRenderCellParams) => (
            <>
            <Link to={`/edit-user/${params.row.id}`}><Button variant="contained" color="primary">
                Изменить
            </Button></Link>
            <Button variant="contained" color="error" sx={{marginLeft: "7px"}} onClick={() => clientStore.deleteUser(params.row.id, setLoading)}>
            Уволить
            </Button></>
        )})
    }

    return (
        <div style={{ height: 400, width: '90%', display: 'flex', justifyContent: "center", marginTop: "3vh", borderRadius: "27px" }}>
            <DataGrid
            rows={clientStore.empUsers ? (clientStore.empUsers): ([])}
            columns={columns}
            initialState={{
                pagination: {
                paginationModel: { page: 0, pageSize: 5 },
                },
            }}
            pageSizeOptions={[5, 10]}
            />
        </div>
    )
}

export default observer(EmpList)