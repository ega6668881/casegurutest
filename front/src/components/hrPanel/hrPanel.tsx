import React, { FC } from 'react'
import EmpList from './empList/empList'
import EmpStats from './stats/empStats'
import Button from '@mui/material/Button/Button';
import { Typography } from '@mui/material'
import { observer } from 'mobx-react-lite';
import './style.css'
import clientStore from '../../stores/clientStore'
import { Link } from 'react-router-dom';
import EmpBirthdays from './usersBirthday/usersBirthday'
  

const HrPanel: FC<{}> = ({}) => {
    
    return (<div className='hr-panel-wrapper'>
        <EmpList edit={true} />
        <Link to={'/add-user'}><Button variant="contained" color='primary' sx={{marginTop: "3vh"}}>Добавить сотрудника</Button></Link>
        <EmpBirthdays />
        <Typography variant='h6' sx={{marginTop: "3vh"}}>Статистика найма сотрудников за прошедщий год</Typography>
        <EmpStats type={'hiring'} />
        <Typography variant='h6' sx={{marginTop: "3vh"}}>Статистика увольнений сотрудников за прошедщий год</Typography>
        <EmpStats type={'dismiss'} />
        
    </div>)
}

export default observer(HrPanel)