import { FC, useEffect, useState } from 'react';
import { LineChart } from '@mui/x-charts/LineChart';
import { observer } from 'mobx-react-lite';
import clientStore from '../../../stores/clientStore';
import Loading from '../../../components/loading/loading';


type PropsType = {
    type: string
}

const xLabels = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
];


const EmpStats: FC<PropsType> = ({type}) => {
    let pData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    const [isLoading, setLoading] = useState(true)
    useEffect(() => {
        clientStore.getStatsEmp(setLoading)
    }, [])

    if (!isLoading) {
        if (type === 'hiring') {
            clientStore.hiringUsersLastYear?.forEach((hiring: any)=> {
                pData[Number(hiring.month) - 1] += 1
            })
        } else {
            clientStore.dismissUsersLastYear?.forEach((hiring: any)=> {
                pData[Number(hiring.month) - 1] += 1
            })
        }
    }
    if (isLoading) {
        return <Loading />
    }

    return (
        <LineChart
        width={1000}
        height={500}
        series={[
            { data: pData, label: 'Сотрудники', yAxisKey: 'leftAxisId' },
        ]}
        xAxis={[{ scaleType: 'point', data: xLabels }]}
        yAxis={[{ id: 'leftAxisId'}]}
        />
    );
}

export default EmpStats