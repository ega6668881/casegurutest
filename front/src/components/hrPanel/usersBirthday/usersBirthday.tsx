import React, { FC, useEffect, useState } from 'react'
import { Typography, Box, Container } from '@mui/material';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import clientStore from '../../../stores/clientStore';
import Loading from '../../loading/loading';
  

const EmpBirthdays: FC<{}> = ({}) => {
    const [loading, setLoading] = useState<boolean>(true)
    
    useEffect(() => {
        clientStore.getBirthdaysUsers(setLoading)
    }, [])

    if (loading) {
        return <Loading />
    }

    return (
        <>{clientStore.birthDaysUsers && (
            <Container component="main" sx={{maxWidth: "50%"}}>
            <Box
                sx={{
                boxShadow: 3,
                borderRadius: 2,
                px: 4,
                py: 6,
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                gap: 3,
                }}>
            <Typography variant='h6'>Пользователи, которые отмечают день рождения в ближайщий месяц</Typography>
            {clientStore?.birthDaysUsers.map(user => <Typography>{`${user.lastName} ${user.firstName} ${user.secondName}`}</Typography>)}
            </Box>
            </Container>
        )}</>
    )
}

export default observer(EmpBirthdays)