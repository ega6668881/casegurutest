import React, { FC, useState } from 'react'
import clientStore from '../../stores/clientStore';
import { Box, Button, Checkbox, Container, FormControlLabel, Grid, Link, TextField, Typography } from '@mui/material';
import Loading from '../loading/loading';
import { observer } from 'mobx-react-lite';

const AddPasswordForm: FC<{}> = ({}) => {
    const [errorMessage, setErrorMessage] = useState<string[]>([""])
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")
    return (
        <Container component="main" maxWidth="sm">
          <Box
            sx={{
              boxShadow: 3,
              borderRadius: 2,
              px: 4,
              py: 6,
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Typography component="h1" variant="h4">
              Здравствуйте, {`${clientStore.user?.firstName}`}
            </Typography>
            <Typography component="h1" variant="h6">
              Придумайте пароль
            </Typography>
            <Box component="form" noValidate sx={{ mt: 1 }} onSubmit={(e) => clientStore.addPassword(e, password, confirmPassword, setErrorMessage)}>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="fullName"
                    label="Пароль"
                    name="fullName"
                    type="password"
                    autoComplete="password"
                    autoFocus
                    onChange={(e) => setPassword(e.target.value)}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="confirmPassword"
                    label="Повторите пароль"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onChange={(e) => setConfirmPassword(e.target.value)}
                />
              
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}>
                    Войти
                </Button>
                <Typography component="h1" variant="h6" sx={{fontSize: "13px"}}>
                    {errorMessage && errorMessage.map(error => <p>{error}</p>)}
                </Typography>
              
            </Box>
          </Box>
        </Container>
    )
}

export default observer(AddPasswordForm)