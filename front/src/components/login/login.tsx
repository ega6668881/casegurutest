import React, { FC, useState } from 'react'
import { Box, Button, Checkbox, Container, FormControlLabel, Grid, Link, TextField, Typography } from '@mui/material';
import clientStore from '../../stores/clientStore';
import { observer } from 'mobx-react-lite';


const LoginForm: FC<{}> = ({}) => {
    const [errorMessage, setErrorMessage] = useState<string>("")
    const [fullName, setFullname] = useState("")
    const [password, setPassword] = useState("")
    return (
        <Container component="main" maxWidth="sm">
          <Box
            sx={{
              boxShadow: 3,
              borderRadius: 2,
              px: 4,
              py: 6,
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Typography component="h1" variant="h5">
              Вход
            </Typography>
            <Box component="form" noValidate sx={{ mt: 1 }} onSubmit={(e) => clientStore.login(e, fullName, password, setErrorMessage)}>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="fullName"
                    label="ФИО"
                    name="fullName"
                    autoComplete="text"
                    autoFocus
                    onChange={(e) => setFullname(e.target.value)}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Пароль"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onChange={(e) => setPassword(e.target.value)}
                />
              
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}>
                    Войти
                </Button>
                <Typography component="h1" variant="h6" sx={{fontSize: "13px"}}>
                    {errorMessage}
                </Typography>
              
            </Box>
          </Box>
        </Container>
    )
}

export default observer(LoginForm)