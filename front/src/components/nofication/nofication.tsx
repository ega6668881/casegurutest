import React, { FC } from 'react'
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import clientStore from '../../stores/clientStore';
import { observer } from 'mobx-react-lite';


const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
  ) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});


const NoficationComponent: FC<{}> = ({}) => {
    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
    
        clientStore.closeNofication();
      };

    return (
        <Stack spacing={2} sx={{ width: '100%' }}>
        <Snackbar open={clientStore.nofication} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity={clientStore.noficationType} sx={{ width: '100%' }}>
            {clientStore.noficationText}
            </Alert>
        </Snackbar>
    </Stack>
    )
}

export default observer(NoficationComponent)