import React, { FC, useEffect } from 'react'
import { Box, Button, Checkbox, Container, FormControlLabel, Grid, TextField, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

const NotFound: FC<{}> = ({}) => {
    return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '100vh'
      }}>
      <Container maxWidth="md">
        <Grid container spacing={2}>
          <Grid xs={6}>
            <Typography variant="h1">
              404
            </Typography>
            <Typography variant="h6">
              Страница не найдена
            </Typography>
            <Link to="/"><Button variant="contained">На главную</Button></Link>
          </Grid>
        </Grid>
      </Container>
    </Box>
    )
}

export default NotFound