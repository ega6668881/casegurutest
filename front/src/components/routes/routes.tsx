import React, { FC } from 'react'
import { Routes, Route } from 'react-router-dom';
import LoginPage from '../../screens/Login';
import MainPage from '../../screens/MainPage';
import Invite from '../../screens/Invite';
import CheckAuth from '../checkAuth/checkAuth';
import NotFound from '../notFound/notFound';
import AddUser from '../../screens/AddUser';
import EditUserPage from '../../screens/EditUser';

const RoutesComponent: FC<{}> = ({}) => {
    return (
        <Routes>
            <Route path='/' element={<CheckAuth Component={<MainPage />} roles={[1, 2]} />}/>
            <Route path='/add-user' element={<CheckAuth Component={<AddUser />} roles={[2]} />}/>
            <Route path='/edit-user/:id' element={<CheckAuth Component={<EditUserPage />} roles={[2]} />}/>
            <Route path='/login' element={<LoginPage />}/>
            <Route path='/invite/:token' element={<Invite />}/>
            <Route path='*' element={<NotFound />} />
        </Routes>
    )
}

export default RoutesComponent