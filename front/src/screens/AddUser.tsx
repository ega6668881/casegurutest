import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import AddUserForm from '../components/addUser/addUser'

const AddUser: FC<{}> = ({}) => {
    return <AddUserForm />
}

export default observer(AddUser)