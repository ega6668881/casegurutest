import React, { FC, useEffect, useState } from 'react'
import { observer } from 'mobx-react-lite'
import EmpPanel from '../components/emplPanel/emplPanel'
import { Navigate, useParams } from 'react-router-dom'
import EditUser from '../components/hrPanel/editUser/editUser'
import clientStore from '../stores/clientStore'
import Loading from '../components/loading/loading'
import NotFound from '../components/notFound/notFound'

const EditUserPage: FC<{}> = ({}) => {
    const {id} = useParams()
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        clientStore.getUser(Number(id), setLoading)
    }, [])

    if (clientStore.user?.id === id) {
        clientStore.sendNofication('Вы не можете редактировать самого себя', 'error')
        return <Navigate to={`/`}/>
    }

    if (loading) {
        return <Loading />

    } else if (!loading && !clientStore.editUser) {
        return <NotFound />

    } else if (clientStore.editUser) {
        return <EditUser user={clientStore.editUser}/>
    }

    return <></>

}

export default observer(EditUserPage)