import React, { FC } from 'react'
import { observer } from 'mobx-react-lite'
import EmpPanel from '../components/emplPanel/emplPanel'

const EmpPanelPage: FC<{}> = ({}) => {
    return <EmpPanel />
}

export default observer(EmpPanelPage)