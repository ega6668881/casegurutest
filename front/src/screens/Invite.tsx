import React, { FC, useEffect, useState } from 'react'
import clientStore from '../stores/clientStore'
import AddPasswordForm from '../components/invite/addPasswordForm'
import { Navigate, useParams } from 'react-router-dom'
import Loading from '../components/loading/loading'
import NotFound from '../components/notFound/notFound'
import { observer } from 'mobx-react-lite';

const Invite: FC<{}> = ({}) => {
    const {token} = useParams()
    const [isLoading, setLoading] = useState(true)
    useEffect(() => {clientStore.checkInviteToken(token, setLoading)}, [])
    if (isLoading) {
        return <Loading />
    }
    if (!isLoading && clientStore.user === null) {
        return <NotFound />
    }
    if (clientStore.user?.password != null) {
        return(<Navigate to={'/'}/>)
    }
    return (<AddPasswordForm />)
}

export default observer(Invite)