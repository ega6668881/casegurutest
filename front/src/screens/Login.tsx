import { FC} from 'react'
import LoginForm from '../components/login/login'
import { observer } from 'mobx-react-lite';
import clientStore from '../stores/clientStore';

type PropsTypes = {
    ReturnComponent?: React.JSX.Element | undefined
}


const LoginPage: FC<PropsTypes> = ({ReturnComponent}) => {
    if (clientStore.isLogin) {
        if (ReturnComponent) {
            return ReturnComponent
        }
    }

    return <LoginForm />
}

export default observer(LoginPage)