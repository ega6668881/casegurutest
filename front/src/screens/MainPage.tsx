import { FC } from 'react';
import { observer } from 'mobx-react-lite';
import HrPanel from '../components/hrPanel/hrPanel';
import clientStore from '../stores/clientStore';
import EmpPanel from '../components/emplPanel/emplPanel';

const MainPage: FC<{}> = ({}) => {
    if (clientStore.user?.role === 2) {
      return <HrPanel />
    } else {
      return <EmpPanel />
    }
}

export default observer(MainPage)