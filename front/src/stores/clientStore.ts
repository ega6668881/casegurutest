import { action, makeAutoObservable } from "mobx";
import React from "react";
import { backendIp } from "../requestConfig";
import { Dayjs } from "dayjs";

export interface IUser {
    id:number
    lastName:string
    firstName:string
    secondName:string
    pay:number
    role:number
    password?:string
    birthday:Date
    created_at:Date | string
    updatedAt:Date
}

interface CheckAuthResponse {
    user: IUser | undefined
    check: boolean
}

interface LoginResponse {
    jwtToken: string
    user: IUser
}

interface AddPasswordResponse {
    user: IUser,
    jwtToken: string
}

interface ErrorResponse {
    message: string[]
    error: string
    statusCode: number
}

interface LastYearEmp extends IUser {
    month: string
}

interface LastYearEmpResponse {
    hiring: LastYearEmp[]
    dismiss: LastYearEmp[]
}

interface AddUserResponse {
    insertedUser: IUser
    inviteUrl: string
}

type NoficationType = 'error' | 'success'

class ClientStore {
    isLogin: boolean = false
    user: IUser | null = null
    empUsers: IUser[] | null = null
    hiringUsersLastYear: LastYearEmp[] | null = null
    dismissUsersLastYear: LastYearEmp[] | null = null
    nofication: boolean = false
    noficationText: string = ''
    noficationType: NoficationType
    editUser: IUser | null = null
    birthDaysUsers: IUser[] | null = null
    constructor () {
        makeAutoObservable(this)
    }
    
    getBirthdaysUsers = action((setLoading: any) => {
        fetch(`${backendIp}/users/get-users-birthday-last-month`, {
            method: 'GET'
        }).then(response => {
            switch(response.status) {
                case 200: {
                    response.json().then((result: IUser[]) => {
                        this.birthDaysUsers = result
                        setLoading(false)
                    })
                    break
                }
                default: {
                    setLoading(false)
                }
            }
        })
    })

    getUser = action((id: number, setLoading: any) => {
        fetch(`${backendIp}/users/get-user/${id}`, {
            method: 'GET'
        }).then(response => {
            switch(response.status) {
                case 200: {
                    response.json().then((result: IUser[]) => {
                        this.editUser = result[0]
                        setLoading(false)
                    })
                    break
                }
                default: {

                }
            }
        })
    })

    sendNofication = action((text: string, type: NoficationType) => {
        this.noficationText = text
        this.noficationType = type
        this.nofication = true
    })

    closeNofication = action(() => {
        this.nofication = false
    })

    checkForm = (fullName: string, pay: number): false | string[] => {
        console.log(fullName)
        const fullNameSplited: string[] = fullName.split(" ")
        console.log(fullNameSplited)
        if (fullNameSplited.length !== 3) {
            this.sendNofication("Некоректное ФИО", 'error')
            return false

        } else if (pay < 1) {
            this.sendNofication("Минимальная зарплата - 1 рубль", 'error')
            return false

        } else { 
            return fullNameSplited
        }
    }

    addUser = action((e: React.FormEvent, fullName: string, pay: number, created_at: Dayjs | null, birthday: Dayjs | null, setErrorMessage: any, setUserAdded: any) => {
        e.preventDefault()
        const fullNameSplited = this.checkForm(fullName, pay)
        if (fullNameSplited) {
            fetch(`${backendIp}/users`, {
                method: 'POST',
                headers: {"content-type": "application/json"},
                body: JSON.stringify({
                    lastName: fullNameSplited[0],
                    firstName: fullNameSplited[1],
                    secondName: fullNameSplited[2],
                    pay: pay,
                    role: 1,
                    created_at: new Date(Number(created_at?.unix()) * 1000),
                    birthday: new Date(Number(birthday?.unix()) * 1000)
                })
            }).then(response => {
                switch(response.status) {
                    case 201: {
                        response.json().then((result: AddUserResponse) => {
                            setUserAdded(result.inviteUrl)
                        })  
                        this.sendNofication("Пользователь добавлен", 'success')
                        break
                    }
                    default: {
                        response.json().then((result: ErrorResponse) => {
                            console.log(result)
                            setErrorMessage(result)
                        })
                    }
                }
            })
        }
    })

    updateUser = action((e: React.FormEvent, id: number | undefined, fullName: string, pay: number, setEditedUser: any) => {
        e.preventDefault()
        console.log(id)
        const fullNameSplited = this.checkForm(fullName, pay)
        console.log(fullNameSplited)
        if (fullNameSplited) { 
            fetch(`${backendIp}/users/update-user`, {
                method: 'PUT',
                headers: {"content-type": "application/json"},
                body: JSON.stringify({
                    id: id,
                    lastName: fullNameSplited[0],
                    firstName: fullNameSplited[1],
                    secondName: fullNameSplited[2],
                    pay: pay,
                })
            }).then(response => {
                switch(response.status) {
                    case 200: {
                        response.json().then((result: IUser[]) => {
                            this.sendNofication('Пользователь изменен', 'success')
                            setEditedUser(true)
                        })
                        break
                    } default: {
                        console.log(response.status)
                    }
                }
            })
        }
    })

    deleteUser = action((id: number, setLoading: any) => {
        if (id === clientStore.user?.id) {
            this.sendNofication("Вы не можете уволить самого себя", 'error')
        } else {
            fetch(`${backendIp}/users/delete-user/${id}`, {
                method: 'DELETE',
            }).then(response => {
                switch(response.status) {
                    case 200 || 201: {
                        if (this.empUsers) {
                            for (var i = this.empUsers.length - 1; i >= 0; --i) {
                                if (this.empUsers[i].id === id) {
                                    this.empUsers.splice(i,1);
                                }
                            }
                            setLoading(true)
                            this.sendNofication("Пользователь уволен!", 'success')
                        }
                        break
                    }
                    default: {
                        console.log(response.json())
                    }
                }
            })
        }
        
    })

    getUsers = action((setLoading: any) => {
        fetch(`${backendIp}/users/get-users`, {method: 'GET'}).then(response => {
            switch(response.status) {
                case 200 || 201: {
                    response.json().then((result: IUser[]) => {
                        this.empUsers = result
                        setLoading(false)
                    })
                    break
                }
                default: {
                    setLoading(false)
                }
            }
        })
    })

    getStatsEmp = action((setLoading: any) => {
        fetch(`${backendIp}/users/get-emp-year`)
        .then(response => {
            switch(response.status){
                case 200 || 201: {
                    response.json().then((result: LastYearEmpResponse) => {
                        this.hiringUsersLastYear = result.hiring
                        this.dismissUsersLastYear = result.dismiss
                        setLoading(false)
                    })
                    break
                }

                default: {
                    setLoading(false)
                }
            }
        })
    })

    checkAuth = action((setLoading: any) => {
        const token = localStorage.getItem('jwtToken')
        if (token) {
            fetch(`${backendIp}/users/check-auth`, {
                method: 'GET',
                headers: {'jwt': token}
            }).then(response => {
                if (response.status === 200) {
                    response.json().then((result: CheckAuthResponse) => {
                        if (result.user) this.user = result.user
                        else this.user = null
                        this.isLogin = result.check
                    })
                }
            })
            .finally(() => {setLoading(false)})
        } else {
            this.user = null
            this.isLogin = false
            setLoading(false)
        }
        
    })

    checkInviteToken = action((token: string | undefined, setLoading: any): void => {
        fetch(`${backendIp}/users/invites/${token}`).then(response => {
            switch(response.status) {
                case 200: {
                    response.json().then((result: IUser[]) => {
                        this.user = result[0]
                    })
                    break
                }
                default: {
                    response.json().then((result: ErrorResponse) => {

                    })
                    break
                }
            }
            setLoading(false)
        })
    })

    addPassword = action((e: React.FormEvent, password: string, confirmPassword: string, setErrorMessage: any): void => {
        e.preventDefault()
        if (password !== confirmPassword) {
            setErrorMessage(["Пароли не совпадают"])
        } else {
            fetch(`${backendIp}/users/add-password`, {
                method: 'POST',
                headers: {"content-type": "application/json"},
                body: JSON.stringify({
                    password: password,
                    confirmPassword: confirmPassword,
                    idUser: this.user?.id
                })
            })
            .then(response => {
                console.log(response.status)
                switch(response.status) {
                    case 200 | 201: {
                        response.json().then((result: AddPasswordResponse) => {
                            this.user = result.user
                            this.isLogin = true
                            localStorage.setItem('jwtToken', result.jwtToken)
                        })
                        break
                    }
                    default: {
                        response.json().then((result: ErrorResponse) => {
                            setErrorMessage(result.message)
                        })
                        break
                    }
                }
            })        
        }
    })

    login = action((e: React.FormEvent, fullName: string, password: string, setErrorMessage: any): void => {
        e.preventDefault()
        fetch(`${backendIp}/users/login`, {
            method: 'POST',
            headers: {"content-type": "application/json"},
            body: JSON.stringify({
                fullName: fullName,
                password: password
            })
        }).then(response => {
            switch(response.status) {
                case 200 | 201: {
                    response.json().then((result: LoginResponse) => {
                        this.user = result.user
                        this.isLogin = true
                        localStorage.setItem('jwtToken', result.jwtToken)
                    })
                    break
                }
                    
                default: {
                    response.json().then((result: ErrorResponse) => {
                        setErrorMessage(result.message)
                    })
                    break
                }
                    
            }
        })
    })

}


// const clientStore: object = {
//     ClientStore: new ClientStore(),
// }

// export const StoreContext = createContext(clientStore);

// export const useStore = () => {
//   return useContext(StoreContext);
// };

const clientStore = new ClientStore();

export default clientStore